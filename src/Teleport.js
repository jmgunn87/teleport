  function Teleport() {
    this.prevStack = [];
    this.nextStack = [];
  }

  Teleport.prototype = {

    push: function (action) {
      if (this.nextStack.length) this.nextStack = [];
      action.execute.apply(action.context, action.params || []);
      this.prevStack.push(action);
    },

    forward: function () {
      if (!this.nextStack.length) return;
      var action = this.nextStack.pop();
      action.execute.apply(action.context, action.params || []);
      this.prevStack.push(action);
    },

    back: function () {
      if (this.prevStack.length > 0) { 
        var action = this.prevStack.pop();
        action.restore.call(action.context);
        this.nextStack.push(action);
      } 
    },

    clear: function () {
      this.prevStack = [];
      this.nextStack = [];
    }

  };