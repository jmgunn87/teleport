describe("Teleport", function () {

  var instance, 
      commands, 
      context = {
        name: "start"
      };

  beforeEach(function () {
    instance = new Teleport();
    var execute = function (name) { this.prev = this.name; this.name = name;  };
    var restore = function (name) { this.name = this.prev; };
    commands = {
      c1: {
        params: ["c1-execute"],
        context: context,
        execute: execute,
        restore: restore
      },
      c2: {
        params: ["c2-execute"],
        context: context,
        execute: execute,
        restore: restore
      },
      c3: {
        params: ["c3-execute"],
        context: context,
        execute: execute,
        restore: restore
      }
    };
  });

  describe("#push", function () {
    it("adds a command to the stack, executing it", function () {
      instance.push(commands.c1);
      expect(context.name).toBe("c1-execute");
      instance.push(commands.c2);
      expect(context.name).toBe("c2-execute");
      instance.push(commands.c3);
      expect(context.name).toBe("c3-execute");
    });
  });

  describe("#forward", function () {
    it("executes the next command on the lookahead stack", function () {
      instance.push(commands.c1);
      instance.push(commands.c2);
      instance.push(commands.c3);
      instance.back();
      instance.forward();
      expect(context.name).toBe("c3-execute");
    });

    it("does nothing if there are no forward commands to execute", function () {
      instance.push(commands.c1);
      instance.push(commands.c2);
      instance.back();
      instance.push(commands.c3);
      instance.forward();
      expect(context.name).not.toBe("c2-execute");
    });
  });

  describe("#back", function () {
    it("reverses a command", function () {
      instance.push(commands.c1);
      instance.push(commands.c2);
      instance.push(commands.c3);
      instance.back();
      expect(context.name).toBe("c2-execute");
    });
  });

  describe("#clear", function () {
    it("clears all previous and next stacks", function () {
      instance.push(commands.c1);
      instance.push(commands.c2);
      instance.push(commands.c3);
      instance.back();
      instance.clear();
      expect(instance.prevStack.length).toBe(0);
      expect(instance.nextStack.length).toBe(0);
    });
  
  });
});
